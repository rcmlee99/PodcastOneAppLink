//
//  SecondViewController.swift
//  PodcastOne AppLink
//
//  Created by Roger Lee on 3/3/20.
//  Copyright © 2020 SCA Digital Pty Ltd. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    var incomingURL: URL?
    @IBOutlet var urlLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        urlLabel.text = incomingURL?.query
    }


}

