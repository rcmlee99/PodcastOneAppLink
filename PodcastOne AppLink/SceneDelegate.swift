//
//  SceneDelegate.swift
//  PodcastOne AppLink
//
//  Created by Roger Lee on 3/3/20.
//  Copyright © 2020 SCA Digital Pty Ltd. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        //do something
        
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let incomingURL = userActivity.webpageURL,
            let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true) else {
            return
        }

        print("incomingURL = \(incomingURL)")
        print("components = \(components)")
        print("code = \(String(describing: incomingURL["code"]))")
        print("state = \(String(describing: incomingURL["state"]))")
        
        if let windowScene = scene as? UIWindowScene {
            
            let window = UIWindow(windowScene: windowScene)
            let tabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBarScreen") as! UITabBarController
            
            if components.path == "/applink" {
                tabBarController.selectedIndex = 2
                let presentingVC = tabBarController.viewControllers![2] as! LinkingStatusViewController
                presentingVC.incomingURL = incomingURL
                
            } else if components.path == "/oauth2" {
                tabBarController.selectedIndex = 2
                let presentingVC = tabBarController.viewControllers![2] as! LinkingStatusViewController
                presentingVC.incomingURL = incomingURL
            
            } else if components.path == "/podcasts/hamish-andy" {
                tabBarController.selectedIndex = 3
                let presentingVC = tabBarController.viewControllers![3] as! LibraryTableViewController
                presentingVC.incomingURL = incomingURL
                
            } else if components.path == "/podcasts/health-hacker" {
                tabBarController.selectedIndex = 3
                let presentingVC = tabBarController.viewControllers![3] as! LibraryTableViewController
                presentingVC.incomingURL = incomingURL
               
            } else if components.path == "/__/auth/action" {
                tabBarController.selectedIndex = 1
                let presentingVC = tabBarController.viewControllers![1] as! SecondViewController
                presentingVC.incomingURL = incomingURL
            }
   
            window.rootViewController = tabBarController
            self.window = window
            window.makeKeyAndVisible()
        }
    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        guard let _ = (scene as? UIWindowScene) else { return }
        
        // Determine who sent the URL.
        if let urlContext = connectionOptions.urlContexts.first {
                
            let sendingAppID = urlContext.options.sourceApplication
            let url = urlContext.url
            print("source application = \(sendingAppID ?? "Unknown")")
            print("url = \(url)")
                
            // Process the URL similarly to the UIApplicationDelegate example.
            if let windowScene = scene as? UIWindowScene {

                let window = UIWindow(windowScene: windowScene)
                
                let initialViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabBarScreen") as! UITabBarController
                initialViewController.selectedIndex = 2
                window.rootViewController = initialViewController

                self.window = window
                window.makeKeyAndVisible()
            }
        }
        
        
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }

}

extension URL {
    subscript(queryParam:String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParam })?.value
    }
}
