//
//  LibraryTableViewController.swift
//  Podcast One
//
//  Created by Roger Lee on 2/12/19.
//  Copyright © 2019 Roger Lee. All rights reserved.
//

import UIKit

class LibraryTableViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var mainImageView: UIImageView!
    var incomingURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let components = NSURLComponents(url: incomingURL!, resolvingAgainstBaseURL: true) {
            if components.path == "/podcasts/hamish-andy" {
                mainImageView.image = UIImage(named: "smallcard_hamish")! as UIImage
            } else if components.path == "/podcasts/health-hacker" {
                mainImageView.image = UIImage(named: "smallcard_hacker")! as UIImage
            }
        }
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }


}
