//
//  FirstViewController.swift
//  PodcastOne AppLink
//
//  Created by Roger Lee on 3/3/20.
//  Copyright © 2020 SCA Digital Pty Ltd. All rights reserved.
//

import SafariServices
import Alamofire
import AuthenticationServices
import FirebaseUI

/*! @brief The OIDC issuer from which the configuration will be discovered.
 */

// Alexa Link Consent
// https://alexa.amazon.com/spa/skill-account-linking-consent?fragment=skill-account-linking-consent&client_id={ClientId}&scope=alexa::skills:account_linking&skill_stage={skillStage}&response_type=code&redirect_uri={yourRedirectUrl}&state={yourState}

// Amazon Link Consent
// https://www.amazon.com/ap/oa?client_id={ClientId}&scope=alexa::skills:account_linking&skill_stage={skillStage}&response_type=code&redirect_uri={yourRedirectUrl}&state={yourState}

/*! @brief The OAuth client ID and client Secret
 */
let kClientID = "amzn1.application-oa2-client.313fcc21e9204cc7b65af0a14b12dcfa";
let kClientSecret = "be49d6d62e72004906f126011a2447ba150a2a60bdc414e9068a2e23f02996b3";

/*! @brief The OAuth redirect URI for the client
 */
let kRedirectURI = "https://alexa-dev-link.firebaseapp.com/applink";

/*! @brief The Alexa Skill ID
*/
let kSkillID = "amzn1.ask.skill.5af8ef25-f877-4c50-b65a-920c6ba4f257";

/*! @brief The Firebase API for SendVerifyEmail
*/
let kBasicClientCredential = "dm9pY2VhcHBAc2NhLmNvbS5hdTp0aGlzaXNtb2Nra2V5b25seQ==";
let kAPISendVerifyEmailEndpoint = "https://us-central1-test-podcastone.cloudfunctions.net/apiverifyemail"
let kAPIAuthCodeEndpoint = "https://us-central1-test-podcastone.cloudfunctions.net/oauth2-apiauthcode"

class FirstViewController: UIViewController, FUIAuthDelegate, ASWebAuthenticationPresentationContextProviding {

    @IBOutlet var signInButton: UIButton!
    @IBOutlet var linkButton: UIButton!
    @IBOutlet var statusLabel: UILabel!
    let authUI = FUIAuth.defaultAuthUI()
    var oauthcode:String?
    var oauthstate:String?
    var accessToken:String?
    var authURL:URL?
    let uuid = UUID().uuidString
    // Firebase User Info
    var firebaseUserId:String?
    var firebaseUserEmail:String?
    var aswebAuthSession: ASWebAuthenticationSession?
    var context: ASWebAuthenticationPresentationContextProviding?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authUI?.delegate = self
        context = self
        verifyConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        firebaseUserId = UserDefaults.standard.string(forKey: "firebaseUserId")
        firebaseUserEmail = UserDefaults.standard.string(forKey: "firebaseUserEmail")
        if (firebaseUserId != nil) {
            statusLabel.text = "Authenticated User"
        } else {
            statusLabel.text = "UnAuthenticated User"
        }
    }
    
    func verifyConfig() {
        assert(kClientID != "","Update kClientID with your own client ID.")
        assert(kClientSecret != "","Update kClientSecret with your own client Secret.")
        assert(kRedirectURI != "","Update kRedirectURI with your own redirect URI.")
        assert(kSkillID != "","Update kSkillID with your Alexa Amazon Skill ID.")
        assert(kBasicClientCredential != "","Update kBasicClientCredential with Firebase API Client Credential")
        assert(kAPISendVerifyEmailEndpoint != "","Update kAPISendVerifyEmailEndpoint with Firebase API Endpoint")
    }
    
    @IBAction func signInAction(_ sender: Any) {
        let authViewController = (authUI?.authViewController())!
        present(authViewController, animated: true, completion: nil)
    }
    
    @IBAction func signOutAction(_ sender: Any) {
        firebaseUserId = nil
        firebaseUserEmail = nil
        UserDefaults.standard.removeObject(forKey: "firebaseUserId")
        UserDefaults.standard.removeObject(forKey: "firebaseUserEmail")
        statusLabel.text = "UnAuthenticated User"
    }
    
    func isUserSignIn() -> Bool {
        if (firebaseUserEmail == nil) && (firebaseUserId == nil) {
            let alertController = UIAlertController(title: "Error", message:
                "Please sign in", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
            present(alertController, animated: true, completion: nil)
            return false
        }
        return true;
    }
    
    @IBAction func verifyEmailAction(_ sender: Any) {
        if (!isUserSignIn()) { return }
        let parameters = [
            "email": firebaseUserEmail
        ]
        let headers: HTTPHeaders = [
            "Authorization": "Basic " + kBasicClientCredential,
            "Content-Type": "application/json"
        ]
        AF.request(kAPISendVerifyEmailEndpoint, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseString { response in switch response.result {
        case .success(let Resp):
            print("Success with JSON: \(Resp)")
        case .failure(let error):
            print("Request failed with error: \(error)")
            }
        }
    }
    
    @IBAction func linkAction(_ sender: UIButton) {
        authoriseFirebaseServer(sender)
    }
    
    // MARK: Firebase AuthUI
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        if let _ = user {
            print("displayName :\(String(describing: user?.displayName))")
            print("uid :\(String(describing: user?.uid))")
            print("email :\(String(describing: user?.email))")
            firebaseUserId = user?.uid
            firebaseUserEmail = user?.email
            UserDefaults.standard.set(user?.uid, forKey: "firebaseUserId")
            UserDefaults.standard.set(user?.email, forKey: "firebaseUserEmail")
            statusLabel.text = "Authenticated User"
        } else if let _ = error {
            // Handle Eror
            print("Request failed with error: \(String(describing: error))")
        }
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        if let _ = authDataResult {
            let credential = authDataResult?.credential as? OAuthCredential
            print("idToken :\(String(describing: credential?.idToken))")
            print("accessToken :\(String(describing: credential?.accessToken))")
            print("userInfo :\(String(describing: authDataResult?.additionalUserInfo!.profile))")
        } else if let _ = error {
            // Handle Eror
            print("Request failed with error: \(String(describing: error))")
        }
    }
    
    // MARK: Firebase OIDC

    func authoriseFirebaseServer(_ sender: UIButton) {
        
        let appLinkUrl = "https://ghome-dev-link.web.app?client_id=alexa&state=" + uuid + "&scope=profile&response_type=code&redirect_uri=" + kAlexaRedirectURI
        authURL = URL(string: appLinkUrl)
        
        print("Start authoriseFirebaseServer URL ::: \(String(describing: authURL?.absoluteString))")
          
        // Call to your backend to get the authorization request URLs (Alexa app / LWA)
        self.aswebAuthSession = ASWebAuthenticationSession.init(url: authURL!, callbackURLScheme: kAlexaRedirectURI, completionHandler: { (callBack:URL?, error:Error?) in

            // handle auth response
            guard error == nil, let successURL = callBack else {
              return
            }
              
            print("Success authoriseFirebaseServer URL :::", successURL.absoluteString)
            print("code: \(String(describing: successURL["code"]))")
            print("state: \(String(describing: successURL["state"]))")
            UserDefaults.standard.set(successURL["code"], forKey: "useroauthcode")
            UserDefaults.standard.set(successURL["state"], forKey: "useroauthstate")
            self.dismiss(animated: true) {
                self.authoriseAmazon(sender)
            }
            
        })
        self.aswebAuthSession?.presentationContextProvider = context
        self.aswebAuthSession?.prefersEphemeralWebBrowserSession = true
        self.aswebAuthSession?.start()
    }
    
    @IBAction func getAuthCode(_ sender: UIButton) {
        if (!isUserSignIn()) { return }
        let parameters = [
            "uid": firebaseUserId,
            "state": uuid
        ]
        let headers: HTTPHeaders = [
            "Authorization": "Basic " + kBasicClientCredential,
            "Content-Type": "application/json"
        ]
        AF.request(kAPIAuthCodeEndpoint, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in switch response.result {
        case .success(let JSON):
            print("Success with JSON: \(JSON)")
            let responseResult = JSON as! NSDictionary
            print("code: \(String(describing: responseResult["code"]))")
            print("state: \(String(describing: responseResult["state"]))")
            UserDefaults.standard.set(responseResult["code"], forKey: "useroauthcode")
            UserDefaults.standard.set(responseResult["state"], forKey: "useroauthstate")
            self.authoriseAmazon(sender)
        case .failure(let error):
            print("Request failed with error: \(error)")
            }
        }
    }
    
    // MARK: Amazon OIDC
    
    func authoriseAmazon(_ sender: UIButton) {
    
        var appLinkUrl: String?
    
        // Alexa App Install
        if (sender.tag == 0) {
            appLinkUrl = "https://alexa.amazon.com/spa/skill-account-linking-consent?fragment=skill-account-linking-consent&skill_stage=development&client_id=" + kClientID + "&state=" + uuid + "&scope=alexa::skills:account_linking&response_type=code&redirect_uri=" + kRedirectURI
        // No Alexa App Install
        } else if (sender.tag == 1) {
            appLinkUrl = "https://www.amazon.com/ap/oa/?skill_stage=development&client_id=" + kClientID + "&state=" + uuid + "&scope=alexa::skills:account_linking&response_type=code&redirect_uri=" + kRedirectURI
        }
        authURL = URL(string: appLinkUrl!)
        print("Start authoriseAmazon URL :::", authURL!.absoluteString)
        
        // Call to your backend to get the authorization request URLs (Alexa app / LWA)
        self.aswebAuthSession = ASWebAuthenticationSession.init(url: authURL!, callbackURLScheme: kRedirectURI, completionHandler: { (callBack:URL?, error:Error?) in
            
            // handle auth response
            guard error == nil, let successURL = callBack else {
                return
            }
            
            let oauthToken = NSURLComponents(string: (successURL.absoluteString))?.queryItems?.filter({$0.name == "code"}).first
            print(oauthToken ?? "No OAuth Token")
            print("SuccessURL :::", successURL.absoluteString)
            
        })
        
        self.aswebAuthSession?.presentationContextProvider = context
        self.aswebAuthSession?.prefersEphemeralWebBrowserSession = true
        self.aswebAuthSession?.start()
    }
    
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return self.view.window ?? ASPresentationAnchor()
    }
    
}


