//
//  LinkingStatusViewController.swift
//  PodcastOne AppLink
//
//  Created by Roger Lee on 4/3/20.
//  Copyright © 2020 SCA Digital Pty Ltd. All rights reserved.
//

import UIKit
import Alamofire
import AuthenticationServices

let kAlexaRedirectURI = "podcastone://oauth"

class LinkingStatusViewController: UIViewController, ASWebAuthenticationPresentationContextProviding {
    
    var incomingURL: URL?
    var oauthcode:String?
    var oauthstate:String?
    var accessToken:String?
    var userAlexaApiEndpoint:String?
    var authURL:URL?
    let uuid = UUID().uuidString
    var aswebAuthSession: ASWebAuthenticationSession?
    var context: ASWebAuthenticationPresentationContextProviding?
    @IBOutlet var urlLabel: UILabel!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        context = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        urlLabel.text = incomingURL?.query
        
        if let components = NSURLComponents(url: incomingURL!, resolvingAgainstBaseURL: true) {
            if components.path == "/applink" {
                // Coming from Amazon Auth Server
                print("Incoming Amazon Auth URL: \(String(describing: incomingURL))")
                if let respAuthCode = incomingURL!["code"], let respState = incomingURL!["state"] {
                    oauthcode = respAuthCode
                    oauthstate = respState
                    exchangeToken()
                }
            }
        }
    }
    
    func exchangeToken() {
        
        let parameters = [
            "client_id": kClientID,
            "client_secret" : kClientSecret,
            "grant_type" : "authorization_code",
            "redirect_uri" : kRedirectURI,
            "state" : oauthstate!,
            "code" : oauthcode!
        ]
        
        let url = "https://api.amazon.com/auth/o2/token"
        
        print("exchangeToken")
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseJSON
        { response in switch response.result
            {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    let responseResult = JSON as! NSDictionary
                    self.accessToken = responseResult["access_token"] as? String
                    self.getUserAlexaApiEndpoint()
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
            }
        }
    }
    
    func getInfo() {
        
        let url = "https://" + userAlexaApiEndpoint! + "/user/profile"
        
        let httpHeaders : HTTPHeaders = [ "Authorization" : "Bearer " + accessToken! ]
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: httpHeaders).responseJSON
        { response in switch response.result
            {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                case .failure(let error):
                    print("Request failed with error: \(error)")
            }
        }
        
    }
    
    func getUserAlexaApiEndpoint() {
        
        spinner.startAnimating()
        let url = "https://api.fe.amazonalexa.com/v1/alexaApiEndpoint"
        
        let httpHeaders : HTTPHeaders = [ "Authorization" : "Bearer " + accessToken! ]
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: httpHeaders).responseJSON
        { response in switch response.result
            {
                case .success(let JSON):
                    print("Success with JSON: \(JSON)")
                    let responseResult = JSON as! NSDictionary
                    print("Endpoints: \(String(describing: responseResult["endpoints"]))")
                    let endPoints = responseResult["endpoints"] as? Array<String>
                    self.userAlexaApiEndpoint = endPoints?.first
                    print("User Endpoint: \(self.userAlexaApiEndpoint!)")
                    self.skillEnalblement()
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
            }
        }
        
    }
    
    func skillEnalblement() {
        
        let url = "https://" + userAlexaApiEndpoint! + "/v1/users/~current/skills/" + kSkillID + "/enablement"
        print("Skill URL ::: \(url)")
        
        let parameters: [String: Any] = [
            "stage": "development",
            "accountLinkRequest": [
                "redirectUri" : kAlexaRedirectURI,
                "type" : "AUTH_CODE",
                "authCode" : UserDefaults.standard.string(forKey: "useroauthcode") ?? ""
            ]
        ]
        print("parameters ::: \(parameters)")
        
        let httpHeaders: HTTPHeaders = [
            "Authorization": "Bearer " + accessToken!,
            "Content-Type": "application/json"
        ]
        print("httpHeaders ::: \(httpHeaders)")

        print("API skillEnablement")
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: httpHeaders).responseJSON
        { response in switch response.result
            {
                case .success(let JSON):
                    self.spinner.stopAnimating()
                    print("success ::: \(JSON)")
                    let responseResult = JSON as! NSDictionary
                    
                    var jsonData: NSData?
                    do {
                        jsonData = try JSONSerialization.data(withJSONObject: responseResult, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
                        let resp = NSString(data: jsonData! as Data, encoding: String.Encoding.utf8.rawValue)! as String
                        print("Resp: \(resp)")
                        self.urlLabel.text = resp
                    } catch _ {
                        jsonData = nil
                    }
                    
                    //self.urlLabel.text = "Success Link"
                    break

                case .failure(let error):
                    self.spinner.stopAnimating()
                    print("failure ::: \(error)")
                    self.urlLabel.text = "Fail Link"
            }
        }
        
    }
    
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
           return self.view.window ?? ASPresentationAnchor()
    }
}
